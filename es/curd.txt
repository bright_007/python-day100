sum求和查询
curl -H "Content-Type:application/json"  -uelastic:185882 -XGET "http://es.python3.work:1027/chengjiao/_doc/_search?" -d '{                                    "size":0,"aggs":{"return_chengjiao":{"sum":{"field":"chengjiao"}}
}
}'
单个查询
curl -H "Content-Type:application/json"  -uelastic:185882 -XGET "http://es.python3.work:1027/chengjiao/_doc/_search?" -d '{                                    "size":0,"query":{"match":{"name":"李慧明"}}
}'
模糊查询
curl -H "Content-Type:application/json"  -uelastic:1858825 -XGET "http://es.python3.work:1027/kbs-user/_doc/_search?" -d '{                                    "size":5,"query":{"wildcard":{"name":{"value":"*权"}}}
}'
添加数据
curl -H "Content-Type:application/json"  -uelastic:185882 -XGET "http://es.python3.work:1027/chengjiao/_doc/_search?{                                    "name":"0","mobile":"13112345678","sex":"男"}”
添加索引
curl -H "Content-Type:application/json"  -uelastic:185882 -XPUT "http://es.python3.work:1027/chengjiao/_doc/index"
删除索引
curl -H "Content-Type:application/json"  -uelastic:185882 -XDELETE "http://es.python3.work:1027/chengjiao/_doc/index"
更新数据
curl -H "Content-Type:application/json"  -uelastic:185882 -XPOST "http://es.python3.work:1027/chengjiao/_doc/index"
局部更新
POST /website/blog/1/_update
{
	"script" : "ctx._source.views+=1"
}